import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontSize: {
        "base-mobile": ["28px", { lineHeight: "40px" }],
        "base-desktop": ["38px", { lineHeight: "52px" }],
        mobile: ["50px", { lineHeight: "52px" }],
        desktop: ["58px", { lineHeight: "60px" }],
        "7xl": "77px",
      },
      padding: {
        "19.5": "4.875rem",
        "22": "88px",
        "10": "40px",
      },
      margin: {
        "22": "88px",
        "10": "40px",
      },
      borderRadius: {
        xl: "1rem",
      },
      colors: {
        "custom-orange": "#FF733C",
        "custom-purple": "#5A698C",
        "custom-hover": "#DCE6E6",
      },
      height: {
        "3.4": "3.4rem",
      },
      width: {
        search: "40rem",
      },
      lineHeight: {
        tight: "80px", // Custom lineHeight if needed for your design
      },
      spacing: {
        "15": "60px", // Adds a 'p-15' utility class
      },
      maxHeight: {
        "619": "619px",
        "300": "308px",
      },
      fontWeight: {
        "850": "850",
      },
      fontFamily: {
        'oswald': ['Oswald', 'sans-serif'],
        'opensans': ['Open Sans', 'sans-serif'],
      },
    },
  },
  plugins: [],
};
export default config;
