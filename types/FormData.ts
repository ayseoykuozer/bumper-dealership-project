export interface FormData {
    name: string;
    company: string;
    mobile_phone: string;
    email_address: string;
    postcode: string;
    pay_later: string;
    pay_now: string;
  }
  

  export interface InputErrorState {
    [key: string]: string;
  }
  
  export interface InputSuccessState {
    [key: string]: boolean;
  }
  