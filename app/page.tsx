import Header from "@/app/components/Header";
import FirstFold from "@/app/components/MainPage/FirstFold";
import SecondFold from "@/app/components/MainPage/SecondFold";

export default function Home() {
  return (
    <>
      <Header />
      <FirstFold />
      <SecondFold />
    </>
  );
}
