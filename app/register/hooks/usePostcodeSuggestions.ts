import { useState, useEffect } from 'react';
import { fetchPostcodeSuggestions } from '../services/postcodeService';

const usePostcodeSuggestions = (postcode: string) => {
  const [suggestions, setSuggestions] = useState<string[]>([]);

  useEffect(() => {
    let active = true;
    (async () => {
      const results = await fetchPostcodeSuggestions(postcode);
      if (active) setSuggestions(results);
    })();

    return () => { active = false; };
  }, [postcode]);

  return suggestions;
};

export default usePostcodeSuggestions;
