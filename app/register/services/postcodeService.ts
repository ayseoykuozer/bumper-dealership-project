export const fetchPostcodeSuggestions = async (postcode: string): Promise<string[]> => {
    if (!postcode) return [];
    try {
      const response = await fetch(`https://api.postcodes.io/postcodes/${postcode}/autocomplete`);
      const data = await response.json();
      return data.status === 200 && data.result ? data.result : [];
    } catch (error) {
      console.error("Failed to fetch postcode suggestions", error);
      return [];
    }
  };
  