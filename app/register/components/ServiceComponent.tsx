import React from "react";

import { IconProp } from "@fortawesome/fontawesome-svg-core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faPlus } from "@fortawesome/free-solid-svg-icons";
interface ServiceComponentProps {
  name?: string;
  faIcon?: IconProp;
  selectedService: string | null;
  onSelectService: (service: string | null) => void;
}

const ServiceComponent: React.FC<ServiceComponentProps> = ({
  name,
  faIcon,
  selectedService,
  onSelectService,
}) => {
  const isSelected = (service: string) => {
    return selectedService === service;
  };

  return (
    <div className="flex flex-col justify-start items-start gap-2 w-full md:w-[680px] ">
      <div className="flex flex-col justify-start items-start w-full">
        <div className="flex items-center space-x-2">
          {faIcon && (
            <FontAwesomeIcon
              icon={faIcon}
              className="text-orange-500 text-lg"
            />
          )}
          <span className="font-semibold text-gray-700">{name}</span>
        </div>
        <p className="text-neutral-500 text-sm font-normal">
          Please select the services you’re interested in offering your
          customers
        </p>
      </div>
      <div className="flex justify-start items-start gap-2 w-full">
        <button
          type="button"
          onClick={() => onSelectService("PayNow")}
          className={`w-full md:w-auto px-4 py-3 rounded-full border flex justify-center items-center gap-3 ${isSelected("PayNow") ? "bg-black text-white" : "bg-white text-zinc-900 border-zinc-900"}`}
        >
          <span className="text-base font-normal leading-normal">PayNow</span>
          {isSelected("PayNow") ? (
            <FontAwesomeIcon icon={faCheck} className="text-white text-lg" />
          ) : (
            <FontAwesomeIcon icon={faPlus} className="text-zinc-900 text-lg" />
          )}
        </button>
        <button
          type="button"
          onClick={() => onSelectService("PayLater")}
          className={`w-full md:w-auto px-4 py-3 rounded-full border flex justify-center items-center gap-3 ${isSelected("PayLater") ? "bg-black text-white" : "bg-white text-zinc-900 border-zinc-900"}`}
        >
          <span className="text-base font-normal leading-normal">PayLater</span>
          {isSelected("PayLater") ? (
            <FontAwesomeIcon icon={faCheck} className="text-white text-lg" />
          ) : (
            <FontAwesomeIcon icon={faPlus} className="text-zinc-900 text-lg" />
          )}
        </button>
      </div>
    </div>
  );
};

export default ServiceComponent;
