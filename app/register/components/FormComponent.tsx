"use client";
import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowLeft } from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/navigation";
import { FormData, InputErrorState, InputSuccessState } from '@/types/FormData';
import InputForm from "./InputForm";
import {
  faUser,
  faBuilding,
  faMobileButton,
  faEnvelope,
  faHouse,
  faWrench,
} from "@fortawesome/free-solid-svg-icons";
import SubmitButton from "./SubmitButton";
import axios from "axios";
import ServiceComponent from "./ServiceComponent";
import { validateFormData } from "../utils/formValidation";
import { fetchPostcodeSuggestions } from "../services/postcodeService";

const initialState: FormData = {
  name: "",
  company: "",
  mobile_phone: "",
  email_address: "",
  postcode: "",
  pay_later: "false",
  pay_now: "false",
};

type PostcodeSuggestion = string;


const FormComponent = () => {
  const router = useRouter();

  const [formData, setFormData] = useState<FormData>(initialState);
  const [isActive, setIsActive] = useState(false);
  const [inputErrors, setInputErrors] = useState<InputErrorState>({
    name: "",
    company: "",
    mobile_phone: "",
    email_address: "",
    postcode: "",
  });

  const [inputSuccess, setInputSuccess] = useState<InputSuccessState>({
    name: false,
    company: false,
    mobile_phone: false,
    email_address: false,
    postcode: false,
  });
  const [suggestions, setSuggestions] = useState<PostcodeSuggestion[]>([]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { id, value } = e.target;
    if (id === "postcode") {
      fetchPostcodeSuggestions(value);
    }
    setFormData((prevData) => ({ ...prevData, [id]: value }));
  };


  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      fetchPostcodeSuggestions(formData.postcode);
    }, 500);

    return () => clearTimeout(delayDebounceFn);
  }, [formData.postcode]);



  const handleSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();

    const validationResults = validateFormData(formData);
  
    setInputErrors(validationResults.errors);
    setInputSuccess(validationResults.successes);
  
    setFormData(validationResults.formData);
  
    if (!validationResults.isValid) {
      console.error("Validation failed.");

      setIsActive(false)
      return;
    }
    try {
      const response = await axios.post(
        "https://demo.bumper.co/core/dealership/reg/sandbox",
        formData,
      );
      if (response.status === 200) {
        storeDataInLocalStorage(formData);
        setIsActive(true)
        router.push("/search");
      }
    } catch (error) {
      console.error(error);
    }
  };
  const storeDataInLocalStorage = (data: FormData): void => {
    const dataListString = localStorage.getItem("formDataList");
    const dataList: FormData[] = dataListString
      ? JSON.parse(dataListString)
      : [];

    dataList.push(data);

    localStorage.setItem("formDataList", JSON.stringify(dataList));
    
  };

  const handlePostcodeSelect = (postcode: string) => {
    setFormData((prevData) => ({ ...prevData, postcode }));
    setSuggestions([]);
  };

  const handleServiceSelection = (service: string | null) => {
    const isPayLaterSelected = service === "PayLater";
    const isPayNowSelected = service === "PayNow";
    setFormData((prevData) => ({
      ...prevData,
      pay_later: isPayLaterSelected ? "true" : "false",
      pay_now: isPayNowSelected ? "true" : "false",
    }));
  };
  return (
    <div className="h-full min-h-screen relative bg-custom-purple mx-4 px-4 py-4 sm:mx-8 sm:px-8 sm:py-8 md:mx-16 md:px-16 md:py-16 lg:px-64 lg:py-32">
      <button className="text-white" onClick={() => router.push("/")}>
        <FontAwesomeIcon width={32} icon={faArrowLeft} />
      </button>
      <h1 className="text-3xl font-custom font-bold mb10 text-white">
        Join our network
      </h1>
      <div className="text-white">
        <span style={{ fontWeight: 400 }}>Offer </span>
        <span style={{ fontWeight: 850 }}>PayLater</span>
        <span style={{ fontWeight: 400 }}>
          {" "}
          to split servicing and repair work into monthly instalments -
          interest-free. Use{" "}
        </span>{" "}
        <br />
        <span style={{ fontWeight: 850 }}>PayNow</span>
        <span style={{ fontWeight: 400 }}>
          {" "}
          to take secure payments online.
        </span>
      </div>

      <div className="p-12 bg-white rounded-3xl border border-gray-800 flex flex-col justify-start items-start gap-20">
        <form onSubmit={handleSubmit} className="space-y-4 w-full">
          <InputForm
            id="name"
            name="Name"
            faIcon={faUser}
            value={formData.name}
            onChange={handleChange}
            isError={!!inputErrors.name}
            isSuccess={inputSuccess.name}
            errorMessage={inputErrors.name}
          />
          <InputForm
            id="company"
            name="Company"
            faIcon={faBuilding}
            value={formData.company}
            onChange={handleChange}
            isError={!!inputErrors.company}
            isSuccess={inputSuccess.company}
            errorMessage={inputErrors.company}
          />
          <InputForm
            id="mobile_phone"
            name="Mobile phone number"
            faIcon={faMobileButton}
            value={formData.mobile_phone}
            onChange={handleChange}
            isError={!!inputErrors.mobile_phone}
            isSuccess={inputSuccess.mobile_phone}
            errorMessage={inputErrors.mobile_phone}
          />
          <InputForm
            id="email_address"
            name="Email address"
            faIcon={faEnvelope}
            value={formData.email_address}
            onChange={handleChange}
            isError={!!inputErrors.email_address}
            isSuccess={inputSuccess.email_address}
            errorMessage={inputErrors.email_address}
          />
          <div className="relative">
            <InputForm
              id="postcode"
              name="Postcode"
              faIcon={faHouse}
              placeholder="Start typing to match your address"
              value={formData.postcode}
              onChange={handleChange}
              isError={!!inputErrors.postcode}
              isSuccess={inputSuccess.postcode}
              errorMessage={inputErrors.postcode}
            />
            {suggestions.length > 0 && (
              <ul className="absolute z-10 w-full bg-white border rounded-md border-gray-300 mt-1 max-h-60 overflow-auto">
                {suggestions.map((suggestion, index) => {
                  const matchIndex = suggestion
                    .toLowerCase()
                    .indexOf(formData.postcode.toLowerCase());
                  const beforeMatch = suggestion.slice(0, matchIndex);
                  const matchText = suggestion.slice(
                    matchIndex,
                    matchIndex + formData.postcode.length,
                  );
                  const afterMatch = suggestion.slice(
                    matchIndex + formData.postcode.length,
                  );

                  return (
                    <li
                      key={index}
                      className="p-2 text-black hover:bg-gray-100 cursor-pointer"
                      onClick={() => handlePostcodeSelect(suggestion)} // Use the function to set the input value
                    >
                      {beforeMatch}
                      <strong>{matchText}</strong>
                      {afterMatch}
                    </li>
                  );
                })}
              </ul>
            )}
          </div>

          <ServiceComponent
            name="What services are you interested in?"
            faIcon={faWrench}
            selectedService={
              formData.pay_later === "true"
                ? "PayLater"
                : formData.pay_now === "true"
                  ? "PayNow"
                  : null
            }
            onSelectService={handleServiceSelection}
          />
          <div>
            <SubmitButton isActive={isActive} onChange={handleChange} />
          </div>
        </form>
      </div>
    </div>
  );
};

export default FormComponent;
