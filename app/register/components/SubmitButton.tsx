import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

interface SubmitButtonProps {
  isActive: boolean;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

const SubmitButton: React.FC<SubmitButtonProps> = ({ isActive }) => {
  return (
    <button
      type="submit"
      className={`px-6 py-2 w-full rounded-full font-medium flex items-center justify-center gap-2 transition-all duration-150 ease-in-out ${
        isActive
          ? "bg-black text-white"
          : "bg-green-500 text-white hover:bg-green-700"
      }`}
    >
      Register
      <FontAwesomeIcon icon={faArrowRight} />
    </button>
  );
};

export default SubmitButton;
