import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExclamationCircle,
} from "@fortawesome/free-solid-svg-icons";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

interface InputFormProps {
  id?: string;
  name?: string;
  faIcon?: IconProp;
  placeholder?: string;
  value?: string;
  onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
  isError?: boolean;
  isSuccess?: boolean;
  errorMessage?: string;
}

const InputForm: React.FC<InputFormProps> = ({
  id,
  name,
  faIcon,
  placeholder,
  value,
  onChange,
  isError,
  isSuccess,
  errorMessage,
}) => {
  const inputClass = `w-full px-4 py-2 border rounded-full shadow-sm placeholder-gray-400 focus:outline-none ${isError ? "border-red-500 focus:ring-2 focus:ring-red-500 focus:border-red-500" : isSuccess ? "border-green-500 focus:ring-2 focus:ring-green-500 focus:border-green-500" : "border-gray-300 focus:ring-2 focus:ring-indigo-500 focus:border-indigo-500"}`;

  return (
    <div className="flex flex-col space-y-2">
      <div className="flex items-center space-x-2">
        {faIcon && (
          <FontAwesomeIcon icon={faIcon} className="text-orange-500 text-lg" />
        )}
        <span className="font-semibold text-gray-700">{name}</span>
      </div>

      <div className="relative flex items-center">
        <input
          id={id}
          type="text"
          value={value}
          placeholder={placeholder}
          onChange={onChange}
          className={`form-input text-black pl-10 ${inputClass}`} // Add padding to make space for the icon
        />
        <span
          className={`absolute left-0 inset-y-0 flex items-center pl-3 ${isError ? "error" : "success"}`}
        />
      </div>
      {isError && (
        <div className="flex items-center text-red-500 text-sm">
          <FontAwesomeIcon icon={faExclamationCircle} className="mr-1" />
          {errorMessage}
        </div>
      )}
    </div>
  );
};

export default InputForm;
