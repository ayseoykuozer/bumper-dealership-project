import Header from "@/app/components/Header";
import FormComponent from "./components/FormComponent";

export default function Register() {
  return (
    <div className="bg-custom-purple w-full h-full min-h-screen">
      <Header />
      <FormComponent />
    </div>
  );
}
