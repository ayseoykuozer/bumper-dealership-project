export const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
export const mobilePhoneRegex = /^0(\s*)(7)(\s*)(\d(\s*)){9}$/;
export const alphaNumericRegex = /^[a-z0-9 ]+$/i;

export const validateEmail = (email: string) => emailRegex.test(email);
export const validateMobilePhone = (phone: string) => mobilePhoneRegex.test(phone);
export const validateAlphaNumeric = (text: string) => alphaNumericRegex.test(text);
