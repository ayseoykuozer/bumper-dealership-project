import { FormData, InputErrorState, InputSuccessState } from '@/types/FormData';
import { validateEmail, validateMobilePhone, validateAlphaNumeric, emailRegex, mobilePhoneRegex, alphaNumericRegex } from './validation';
import { removeSpaces } from '@/helpers/removeSpaces';

export const validateFormData = (formData: FormData) => {
        console.log("validate");
        let isValid = true;
        const newErrors: InputErrorState = {
          name: "",
          company: "",
          mobile_phone: "",
          email_address: "",
          postcode: "",
        };
        const newSuccess: InputSuccessState = {
          name: false,
          company: false,
          mobile_phone: false,
          email_address: false,
          postcode: false,
        };
      
        const {
          name,
          company,
          mobile_phone,
          email_address,
          postcode,
          pay_later,
          pay_now,
        } = formData;
    
        if (!emailRegex.test(email_address)) {
          newErrors.email_address = "Invalid email address.";
          isValid = false;
        } else {
          newSuccess.email_address = true;
        }
    
        if (company.length < 2 || company.length > 25) {
          newErrors.company = "Invalid company name.";
          isValid = false;
        } else {
          newSuccess.company = true;
        }
    
        if (!mobilePhoneRegex.test(mobile_phone)) {
          newErrors.mobile_phone = "Invalid mobile phone format.";
          isValid = false;
        } else {
          newSuccess.mobile_phone = true;
        }
    
        if (!alphaNumericRegex.test(name) || name.length > 255) {
          newErrors.name =
            "Name must be alpha-numeric and less than 256 characters.";
          isValid = false;
        } else {
          newSuccess.name = true;
        }
    
        if (!alphaNumericRegex.test(postcode) || postcode.length > 30) {
          newErrors.postcode =
            "Postcode must be alpha-numeric and less than 30 characters.";
          isValid = false;
        } else {
          newSuccess.postcode = true;
        }
    
        if (pay_later !== "true" && pay_now !== "true") {
          alert('Either "Pay Later" or "Pay Now" must be selected.');
        }
    
        const modifiedFormData = {
            ...formData,
            postcode: removeSpaces(postcode), // Apply modifications here
          };
        
          // Return both validation results and modifiedFormData
          return {
            isValid,
            errors: newErrors,
            successes: newSuccess,
            formData: modifiedFormData,
          };
      };
