import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBuilding } from "@fortawesome/free-solid-svg-icons";


interface SearchComponentProps {
    onSearchChange: (searchTerm: string) => void;
  }
  
  const SearchComponent: React.FC<SearchComponentProps> = ({ onSearchChange }) => {
  return (
    <div className="flex w-screen items-center justify-center px-4 py-6">
      <div className="w-full w-search p-8 bg-white rounded-2xl border border-zinc-900 flex flex-col justify-start items-start gap-5">
        <div className="self-stretch h-[79px] flex flex-col justify-start items-start gap-[7px]">
          <div className="self-stretch flex justify-start items-center gap-1.5">
            <FontAwesomeIcon
              icon={faBuilding}
              className="text-orange-500 text-lg h-4 w-4"
            />
            <div className="text-zinc-900 text-base font-bold">
              Search Company
            </div>
          </div>
          <div className="self-stretch flex px-4 py-3 bg-white rounded-[27px] border border-neutral-600 justify-start items-center gap-2.5">
          <input
              className="w-full text-neutral-500 text-base font-normal leading-normal bg-transparent placeholder-neutral-400 outline-none"
              placeholder="Start typing name, company, phone or email for search"
              type="text"
              onChange={(e) => onSearchChange(e.target.value)} // Call onSearchChange on input change
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchComponent;
