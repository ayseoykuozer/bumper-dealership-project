import React from "react";
import ContactCard from "./ContactCard";
import { FormData } from "@/types/FormData";

interface ContactCardListProps {
  contacts: FormData[];
}

const ContactCardList: React.FC<ContactCardListProps> = ({ contacts }) => {
  return (
    <div className="space-y-4 items-center justify-center flex flex-col pb-10 px-4">
      {contacts.map((contact, index) => (
        <ContactCard
          key={index}
          name={contact.name}
          company={contact.company}
          mobile_phone={contact.mobile_phone}
          email_address={contact.email_address}
          postcode={contact.postcode}
        />
      ))}
    </div>
  );
};

export default ContactCardList;
