import React from "react";
interface ContactProps {
  name: string;
  company: string;
  mobile_phone: string;
  email_address: string;
  postcode: string;
}

const ContactCard: React.FC<ContactProps> = ({
  name,
  company,
  mobile_phone,
  email_address,
  postcode,
}) => {
  return (
    // <div className="w-full w-search p-8 bg-white rounded-2xl border border-gray-900 flex flex-col justify-start items-start gap-4">
    //   <div className="text-black text-xl font-bold">{name}</div>
     
     
    <div className="w-full sm:w-search p-8 bg-white rounded-2xl border border-gray-900 flex flex-col justify-start items-start gap-4">
    <div className="text-black text-xl font-bold">{name}</div>
   

      <div className="self-stretch h-px bg-gray-300"></div>

      <div className="w-full flex justify-between items-center">
        <div className="text-gray-900 text-base font-bold">Company</div>
        <div className="text-right text-gray-900 text-base font-light">
          {company}
        </div>
      </div>
      <div className="self-stretch h-px bg-gray-300"></div>

      <div className="w-full flex justify-between items-center">
        <div className="text-gray-900 text-base font-bold">
          Mobile phone number
        </div>
        <div className="text-right text-gray-900 text-base font-light">
          {mobile_phone}
        </div>
      </div>
      <div className="self-stretch h-px bg-gray-300"></div>

      <div className="w-full flex justify-between items-center">
        <div className="text-gray-900 text-base font-bold">Email address</div>
        <div className="text-right text-gray-900 text-base font-light">
          {email_address}
        </div>
      </div>
      <div className="self-stretch h-px bg-gray-300"></div>

      <div className="w-full flex justify-between items-center">
        <div className="text-gray-900 text-base font-bold">Postcode</div>
        <div className="text-right text-gray-900 text-base font-light">
          {postcode}
        </div>
      </div>
      <div className="self-stretch h-px bg-gray-300"></div>
    </div>
  );
};

export default ContactCard;
