"use client";
import Header from "@/app/components/Header";
import ContactCard from "@/app/search/components/ContactCard";
import ContactCardList from "@/app/search/components/ContactCardList";
import SearchComponent from "@/app/search/components/SearchComponent";
import { FormData } from "@/types/FormData";
import React, { useEffect, useState } from "react";


export default function Search() {
  const [contacts, setContacts] = useState<FormData[]>([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [visibleCount, setVisibleCount] = useState(3); // State to track the number of visible items

  useEffect(() => {
    const dataListString = localStorage.getItem("formDataList");
    const dataList: FormData[] = dataListString ? JSON.parse(dataListString) : [];
    setContacts(dataList);
  }, []);

  const handleSearchChange = (query: string) => {
    setSearchQuery(query);
    setVisibleCount(3); 
  };

  const filteredContacts = contacts.filter(contact =>
    contact.company.toLowerCase().includes(searchQuery.toLowerCase())
  );

  const showMoreItems = () => {
    setVisibleCount(prevCount => prevCount + 3); 
  };
  return (
    <>
      <div className="bg-custom-purple w-full min-h-screen">
        <Header />
        <SearchComponent onSearchChange={handleSearchChange} />
        <ContactCardList contacts={filteredContacts.slice(0, visibleCount)} />
       
        {filteredContacts.length > visibleCount && ( 
          <div className="flex justify-center py-4 cursor-pointer" onClick={showMoreItems}>
              Load More...
          </div>
        )}
      </div>
    </>
  );
}
