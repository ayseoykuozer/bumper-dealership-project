import React from "react";

type InfoBoxProps = {
  number: string;
  title: string;
  description: string;
};

const InfoBox: React.FC<InfoBoxProps> = ({ number, title, description }) => {
  return (
    <div className="flex items-start p-2">
      <div className="relative w-6 h-6 mr-2">
        <div className="absolute inset-0 bg-custom-orange rounded-full border border-[#1B1B1B]"></div>
        <div className="absolute w-6 h-5 top-0.5 left-0 flex items-center justify-center text-center text-[#1B1B1B] text-sm font-bold leading-5">
          {number}
        </div>
      </div>
      <div className="flex-1">
        <span className="text-[#1B1B1B] text-base font-bold leading-6">
          {title}
        </span>
        <span className="block text-[#1B1B1B] text-base font-normal leading-6">
          {description}
        </span>
      </div>
    </div>
  );
};

export default InfoBox;
