"use client";
import Image from "next/image";
import React from "react";
import { useRouter } from "next/navigation";

const FirstFold: React.FC = () => {
  const router = useRouter();

  const navigateRegister = () => {
    router.push("/register");
  };

  return (
    <div className="relative text-white text-center main-fold main-fold-inner h-full bg-cover ">
      <div className=" inset-0 bg-black bg-opacity-50 main-fold-inner flex flex-col p-4 lg:px-20 py-10">
        <div className="w-full mt-10 inline-flex gap-3">
          <div className="text-white text-base leading-6 break-words">
            Excellent
          </div>

          <Image
            width={85}
            height={6}
            alt="trustpilot"
            src="stars-trustpilot.svg"
          />

          <Image width={63} height={6} alt="star" src="trustpilot.svg" />
        </div>
        <div className="text-white lg:text-7xl text-left mt-8 font-bold lg:leading-tight break-words text-[38px] lg:text-[77px]">
          BECOME A BUMPER APPROVED DEPENDABLE DEALERSHIP
        </div>
        <p className="mb-8 text-left">
          Join our network of 3,000+ garages and dealerships who already offer
          Bumper to their customers.
        </p>
        <button
          onClick={navigateRegister}
          className="w-[235px] h-full inline-flex items-center justify-center gap-2.5 bg-[#32BE50] rounded-[27px] border border-[#1B1B1B] p-3 z-100"
        >
          <div className="text-center text-[#1B1B1B] text-base font-semibold leading-[19px] break-words">
            Register your interest
          </div>
          <div className="relative w-4 h-[19px]">
            <i className="fas fa-arrow-right absolute text-center text-[#1B1B1B] text-base font-bold left-0 top-[2px]"></i>
          </div>
        </button>
        <p className="text-xs text-left mt-3">
          Already registered?{" "}
          <a href="/login" className="underline">
            Login
          </a>
        </p>
      </div>
    </div>
  );
};

export default FirstFold;
