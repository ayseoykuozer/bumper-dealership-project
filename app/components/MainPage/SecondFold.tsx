"use client";
import Image from "next/image";
import React from "react";
import { useRouter } from "next/navigation";
import InfoBox from "./InfoBox";

const SecondFold: React.FC = () => {
  const router = useRouter();

  const navigateRegister = () => {
    router.push("/register");
  };
  return (
    <div className="flex flex-col md:flex-row items-start gap-12 bg-white w-full h-full p-8 pt-10 px-4 md:p-15">
      <div className="relative items-center self-center flex w-auto h-[300px] md:h-[619px] lg:h-[619px] rmd:w-1/2 md:order-2 lg:order-2 ">
        <Image
          className="relative items-center flex"
          src="/Phone.png"
          alt="logo"
          layout="fill"
          objectFit="cover"
        />
      </div>

      <div className="flex-grow flex flex-col md:w-1/2 md:order-1 lg:order-1 gap-6">
        <div className="flex flex-col gap-4">
          <div className="flex flex-col gap-4">
            <Image src="/BumperLogo.svg" width={110} height={28} alt="logo" />

            <div className="text-left text-[#1B1B1B] text-mobile font-oswald font-bold uppercase break-words md:text-desktop">
              PayLater
            </div>
          </div>
        </div>
        <p className="flex-grow text-black text-xl ">
          Give customers more flexibility at checkout, online and in store. Let
          them spread the cost with interest-free monthly payments.
        </p>
        <div className="flex flex-col gap-4">
          <div className="text-custom-orange font-universal font-850 text-base-mobile leading-none md:text-base-desktop md:leading-none break-words w-full md:w-auto">
            No risk to your business.
          </div>

          <div className="text-custom-orange font-universal font-850 text-base-mobile leading-none md:text-base-desktop md:leading-none break-words w-full">
            No worries for your customers.
          </div>
        </div>
        <div className="text-black text-lg font-bold">It’s as simple as:</div>
        <div className="space-y-4">
          <InfoBox
            number="1"
            title="FIX IT"
            description="Your customers bring their vehicle to you. You repair and service the car. Everything just like it works right now."
          />
          <InfoBox
            number="2"
            title="SPLIT IT"
            description="When the customer gets their bill or quote, your customer chooses the option to ‘PayLater’ and in just a few clicks they’ve been approved and have paid."
          />
          <InfoBox
            number="3"
            title="SORTED"
            description="You and your customer part ways happy. You’re paid upfront, directly from Bumper, the customer repays Bumper over their chosen payment plan."
          />
        </div>
        <button
          onClick={navigateRegister}
          className="flex items-center justify-center gap-2.5 bg-green-600 rounded-full border border-black px-10 py-3 text-center text-black text-xl font-medium leading-8"
        >
          Register your interest
          {/* Arrow icon from FontAwesome needs to be added here */}
        </button>
      </div>
    </div>
  );
};

export default SecondFold;
