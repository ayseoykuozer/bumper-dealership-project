"use client";
import Image from "next/image";
import { useRouter } from "next/navigation";
const customStyles = {
  rotateDiv: {
    alignSelf: "stretch",
    transformOrigin: "0 0",
    border: "1px solid #545454",
  },
  bar: (backgroundColor: string) => ({
    alignSelf: "stretch",
    height: "4px",
    background: backgroundColor,
    borderTopLeftRadius: "4px",
    borderTopRightRadius: "4px",
  }),
};

const Header: React.FC = () => {
  const router = useRouter();

  const navigateHeader = () => {
    router.push("/");
  };
  return (
    <header>
      <div className="relative z-20 w-full h-full inline-flex items-center justify-start gap-6  px-7 sm:px-19.5 bg-black rounded-bl-xl rounded-br-xl">
        <div className="text-center text-white text-sm leading-5 inline-flex flex-col items-start justify-start gap-2 my-3 whitespace-nowrap">
          For business
        </div>

        <div style={customStyles.rotateDiv}></div>

        <div className="text-center text-white text-sm leading-5  inline-flex flex-col items-start justify-start gap-2 my-3 whitespace-nowrap">
          For drivers
        </div>

        <div className="w-full h-full justify-end gap-2 hidden sm:inline-flex">
          <div className="text-center justify-end text-white text-sm leading-5 break-words border border-white rounded px-3 py-1 ">
            Partner login
            <i
              className="fas fa-icon  text-center text-white text-xs"
              style={{
                left: "0px",
                fontWeight: "400",
              }}
            ></i>
          </div>
        </div>
      </div>
      <div className="relative z-10 w-full h-3.4 inline-flex items-center justify-start gap-2 px-7 sm:px-19.5 bg-custom-orange rounded-bl-xl rounded-br-xl m-0 -mt-3 pt-3">
        <Image
          onClick={navigateHeader}
          src="/BumperLogo.svg"
          width={110}
          height={28}
          alt="logo"
        />
        <p className="whitespace-nowrap">for Business</p>
        <div className="w-full justify-end gap-2 inline-flex">
          <div className="text-center justify-end text-white text-sm leading-5 break-words  bg-green-600 rounded border border-gray-900 px-3 py-1 gap-2.5 ">
            <p className="text-center text-gray-900 text-base font-medium leading-6 break-words">
              Register
            </p>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
