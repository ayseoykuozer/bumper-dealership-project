import type { Metadata } from "next";
import { Oswald, Open_Sans } from "next/font/google";
import "./globals.css";

import "@fortawesome/fontawesome-free/css/all.css";

const oswald = Open_Sans({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Bumper",
  description: "Bumper for business",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      
      <body className={oswald.className}>{children}</body>
    </html>
  );
}
